#!/usr/bin/env node
const _ = require('lodash');
const moment = require('moment');
const debug = require('debug')('bin:poster');
const nconf = require('nconf');
 
const event = require('../lib/event2');
const shared = require('../lib/shared');
const location = require('../lib/location');

nconf.argv().env().file({file: "config.json"});

async function poster() {
    
/*    shared.integrityChecks({
        start: 'When the event begin: Format YYYY-MM-DD HH:mm',
        end: 'When the event ends: Format YYYY-MM-DD HH:mm',
        title: 'Event title',
        description: 'Event description, you should supply the "\n" this way, if any',
        url: 'Event URL, or Facebook URL, or leave it empty with ""',
        address: "You should specify a full address — works: 'Street N, City, Region'"
    });
*/
    path = process.argv[2];
    const eventvars = require(path);
    eventvars.location = await location.queryLocation(eventvars.address);
    eventvars.start = moment(eventvars.start);
    if (eventvars.end) {
      eventvars.end = moment(eventvars.end);
    }
    debug(eventvars);
   
    await event.postToMobilizon(eventvars);
    debug("Posted successfully, now listing the last four Event in the node:");

}

poster();
