#!/bin/bash -eu

gql='mutation($file: Upload!, $name:String!){uploadMedia(name: $name, file: $file, alt:null){id}}'

BEARER=$(jq '.token' ../mobilizon-poster/identities.json) 
#BEARER='eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJtb2JpbGl6b24iLCJleHAiOjE2Mjg4NTQzODQsImlhdCI6MTYyNjQzNTE4NCwiaXNzIjoibW9iaWxpem9uIiwianRpIjoiYTU2MzM3NDctM2RlNS00N2M5LWJkNTEtZTZlZWNkYjRmNWYyIiwibmJmIjoxNjI2NDM1MTgzLCJzdWIiOiJVc2VyOjEiLCJ0eXAiOiJhY2Nlc3MifQ.Wfj1NnSOmDsguj05JXgAicndTSCkr2YcEikIrIYGEtsl_eEJ2OSAbjfBFPxzwt0fRWHxRG8xF7vcbr9pzpnJWQ'

MOBILIZON_EP=$(jq '.api' config.json)

printf -v vars '{ "name" : "%s", "file": "thefile" }' $1

curl -sS \
  -H 'accept: application/json' \
  -H "Authorization: Bearer ${BEARER//\"/}" \
  -F "query=${gql}" \
  -F "variables=${vars}" \
  -F "thefile=@${2}" \
  "${MOBILIZON_EP//\"/}"

