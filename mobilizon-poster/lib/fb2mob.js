#!/usr/bin/env node
const _ = require('lodash');
const moment = require('moment');
const debug = require('debug')('bin:poster');
const nconf = require('nconf');

nconf.argv().env().file({file: "config.json"});

const fbevent = require('../../librevent/automation/evdetails/824411411795146_2021-07-27-00-01.json');

async function convert() {
    var result = fbevent.description.replace(/\n\n/g, '</p><p>');
    result = result.replace(/\n/g, '<br>');
//    console.log('Ha: %s', "<p>" + result + "</p>");
    return "<p>" + result + "</p>"; 
}

module.exports = {
    convert
}
