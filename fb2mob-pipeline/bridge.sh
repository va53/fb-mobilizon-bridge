#!/bin/bash

###
# TODOS: document assumptions: berlin system time, UNK tim of FB
#           creation and assumption on json files
#        catch no defined end time
#        check variables for consistency before calling the poster
#        implement time extraction for several days events 
#        skip posting if time format is not valid
#        compare cross posted events with current FB source
#        incomplete list on defaults and assumptions:
#        - when no end date is given: end=start+5hours
####

libreventPATH="../librevent/automation"
fb2mobPATH="../fb2mob-pipeline"
posterPATH="../mobilizon-poster"

cd $libreventPATH

venue=$1
# "about.party"

# make sure no pointer exists
if [ "$(ls pointers)" ]; then
  rm -r pointers/20*
fi
## create pointer
DEBUG=*,-puppeteer*,-follow-redirects node src/guardoni.js --page https://www.facebook.com/$venue/events/ --profile newer
pointer=$(ls pointers)
# delete event details
if [ "$(ls evdetails)" ]; then
  rm -r evdetails/*
fi
DEBUG=*,-puppeteer*,-follow-redirects node src/guardoni.js --profile newer --pointer pointers/$pointer --delay 3
events=($(jq '.eventHrefs[]' pointers/$pointer))

cd $posterPATH
for event in ${events[@]}
do
  ev_id=$(echo $event | sed 's/[^0-9]//g')
# check if event has already been crossposted
  if [ "$(jq --arg id "${ev_id}" '.[$id]' $fb2mobPATH/$venue.json)" = "null" ]; then
   jq --arg id "${ev_id}" '.[$id]={}' $fb2mobPATH/$venue.json > $fb2mobPATH/$venue.json.tmp
   mv $fb2mobPATH/$venue.json.tmp $fb2mobPATH/$venue.json
   file=$(ls $libreventPATH/evdetails/${ev_id}*.json)
   when=$(jq '.when' $file)
   description=$(jq '.description' $file)
   description=$(echo $description | sed 's/\\n\\n/<\/p><p>/g' | sed 's/\\n/<br>/g' |  sed 's/.\{1\}/<p>/' | sed 's/.$/<\/p>/') 
   title=$(jq '.title' $file)
   location=$(jq '.location' $file)
   image=$(jq '.saved' $file)

   # Time Calculations
   # Get system offset to UTC
   # assume system time is Berlin time
   SysOffsetLong=$(date +%z)
   SysOffset=${SysOffsetLong:0:3}
   # offset to FB's UNK to UTC
   FBOffset="7"
   
   totalOffset=$(($FBOffset+$SysOffset))
   
   start=""
   end=""
   # assume format: 31.08.2018 um 13:00 UNK – 14.09.2018 um 01:00 UNK for events longer than a day and 
   # Sonntag, 27. Mai 2018 von 02:00 UNK bis 07:00 UNK for events during a day
   # no end date, at all: Samstag, 14. August 2021 um 13:00 UNK
   if [ -z "$(echo $when | grep "UNK")" ]; then
     echo -e "date format not with expected UNK time specifer\ndo nothing"
   elif [ "$(echo $when | grep -o -i UNK | wc -l)" = "1" ]; then
     echo "no end date"
     datestring=$(echo $when | awk -F, '{print $2}' | awk -F'um' '{print $1}' | sed 's/\.//')
     day_no=$(echo $datestring | sed 's/[^0-9].*//')
     month_abbr=$(echo $datestring | sed 's/[\ \.0-9]//g' | head -c 3  | sed 's/ä/a/' | sed 's/i/y/' | sed 's/k/c/'| sed 's/z/c/' | tr -d '[:space:]')
     year_digit=$(echo $datestring | tr -d '[:space:]' | tail -c 4)
     date=$(date -d"$day_no $month_abbr $year_digit" +%Y-%m-%d)
     
     # get system timezone
     # assume sysTZ is equal to Berlin's TZ
     sysTZ=$(date +%Z)     
     
     startTime=$(echo $when | awk -F, '{print $2}' | awk -F'um' '{print $2}' | awk -F'UNK' '{print $1}' | tr -d '[:space:]')
     start=$(date --date "$date $startTime:00 $sysTZ +$totalOffset hours" +%Y-%m-%d\ %H:%M) 
     end=null;
#     $(date --date "$date $startTime:00 $sysTZ +$((totalOffset+5)) hours" +%Y-%m-%d\ %H:%M) 
     
   elif [ "$(echo $when | grep von)" ]; then
     echo "less than a day"
     datestring=$(echo $when | awk -F, '{print $2}' | awk -F'von' '{print $1}' | sed 's/\.//')
     day_no=$(echo $datestring | sed 's/[^0-9].*//')
     month_abbr=$(echo $datestring | sed 's/[\ \.0-9]//g' | head -c 3  | sed 's/ä/a/' | sed 's/i/y/' | sed 's/k/c/'| sed 's/z/c/' | tr -d '[:space:]')
     year_digit=$(echo $datestring | tr -d '[:space:]' | tail -c 4)
     date=$(date -d"$day_no $month_abbr $year_digit" +%Y-%m-%d)
     
     # get system timezone
     # assume sysTZ is equal to Berlin's TZ
     sysTZ=$(date +%Z)     
     
     startTime=$(echo $when | awk -F, '{print $2}' | awk -F'von' '{print $2}' | awk -F'UNK' '{print $1}' | tr -d '[:space:]')
     endTime=$(echo $when | awk -F, '{print $2}' | awk -F'von' '{print $2}' | awk -F'UNK' '{print $2}' | awk -F'bis' '{print $2}' | tr -d '[:space:]')
     start=$(date --date "$date $startTime:00 $sysTZ +$totalOffset hours" +%Y-%m-%d\ %H:%M) 
     end=$(date --date "$date $endTime:00 $sysTZ +$totalOffset hours" +%Y-%m-%d\ %H:%M)
      
   elif [ -z "$(echo $when | grep ,)" ]; then
     echo "several days"
     exit
   fi
     address=$(jq '.address' $fb2mobPATH/$venue.json)
     url=$(jq '.url' $fb2mobPATH/$venue.json)
     if [[ ! "$end" == "null" ]]; then end=\"$end\"; fi
     cd $posterPATH
     output=$(./uploader.sh "$ev_id" "$libreventPATH/${image//\"/}")
     media_id=null
     if  [ "$output" ]; then
          media_id=$(jq '.data.uploadMedia.id' <<<$output)
     fi
     # missing: attributed_to_id and organizer_id
     # jq '.[3].preferredusername' groups.json <- in this case output is aboutblank
     # jq length groups.json <- use this to loop through entries and search for groupname
     #attributed_to_id=$(jq '.[3].parent.id' groups.json) 
     attributed_to_id=$(jq '.attributedToId' $fb2mobPATH/$venue.json)
     organizer_id=$(jq '.[0].actor.id'  groups.json) 
     
     cd $fb2mobPATH
     echo $end
     jq <<<"{\"start\": \"$start\",\"end\": $end,\"title\": $title,\"description\": \"$description\", \"address\": $address, \"url\": $url, \"picture\": {\"media_id\": $media_id}, \"organizer_id\":$organizer_id, \"attributed_to_id\": $attributed_to_id}"
     
     jq <<<"{\"start\": \"$start\",\"end\": $end,\"title\": $title,\"description\": \"$description\", \"address\": $address, \"url\": $url, \"picture\": {\"media_id\": $media_id}, \"organizer_id\":$organizer_id, \"attributed_to_id\": $attributed_to_id}" > eventvars.json
     # call mobilizon poster
     cd $posterPATH
     node bin/poster2.js $fb2mobPATH/eventvars.json  
  # end of crosspost check
  fi
done
